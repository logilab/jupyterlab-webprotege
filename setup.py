import setuptools

setuptools.setup(
    name="jupyter-webprotege-server",
    packages=setuptools.find_packages(),
    package_data={
        'jupyter_webprotege_server': ['icons/*'],
    },
    entry_points={
        'jupyter_serverproxy_servers': [
            'jupyter_webprotege_server = jupyter_webprotege_server:setup_webprotege',
        ]
    },
    install_requires=['jupyter-server-proxy'],
)
