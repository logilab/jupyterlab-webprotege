import os


def setup_webprotege():
    return {
        'command': ['/usr/local/apache-tomcat9/bin/startup.sh'],
        'timeout': 50,
        'port': 8085,
        'launcher_entry': {
            'icon_path': os.path.join(
                os.path.dirname(os.path.abspath(__file__)),
                'icons',
                'webprotege.svg'
            ),
            'title': "Webprotege",
        }
    }
